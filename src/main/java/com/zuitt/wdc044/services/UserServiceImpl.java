package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired //injects code from classes/files that we have imported
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public ResponseEntity updateUser(Long id, String stringToken, User user){
        User userForUpdating = userRepository.findById(id).get();
        String userAuthor = userForUpdating.getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(userAuthor)){
            userForUpdating.setUsername(user.getUsername());
            userForUpdating.setPassword(user.getPassword());
            userRepository.save(userForUpdating);
            return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to edit this user.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deleteUser(Long id, String stringToken){
        User userForUpdating = userRepository.findById(id).get();
        String userAuthor = userForUpdating.getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(userAuthor)){
            userRepository.deleteById(userForUpdating);
            return new ResponseEntity<>("user deleted successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to delete this user.", HttpStatus.UNAUTHORIZED);
        }
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

}
